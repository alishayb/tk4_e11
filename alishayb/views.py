from django.shortcuts import render, redirect
from alishayb.forms import *
from datetime import date
from django.http import request, JsonResponse
from django.db import connection
from django.urls import reverse
from django.contrib import messages
import json

# Create your views here.
def create_transaksi_sumber_daya(request):
    username = request.session['username']
    if request.session['role'] != 'Petugas Faskes':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        if request.method == "POST":
            form = TransaksiSumberDaya(request.POST)
            now = date.today().strftime("%Y-%m-%d")
            cursor.execute("INSERT INTO TRANSAKSI_SUMBER_DAYA(tanggal) values(\'" + now + "\')")
            cursor.execute('SELECT MAX(nomor) FROM TRANSAKSI_SUMBER_DAYA')
            tuple_no_terakhir = list(cursor.fetchone())
            no_transaksi = int(tuple_no_terakhir[0])

            return redirect('alishayb:create_permohonan_sumber_daya', no_transaksi)

        cursor.execute('SELECT MAX(nomor) FROM TRANSAKSI_SUMBER_DAYA')
        no_terakhir_permohonan = list(cursor.fetchone())
        no_transaksi_baru = int(no_terakhir_permohonan[0]) + 1

        form = TransaksiSumberDaya()
        form.fields['nomor_transaksi'].widget.attrs.update({'value': 'TREQ'+ str(no_transaksi_baru)})
        form.fields['username_petugas'].widget.attrs.update({'value': username})
        context = {
            "permohonan_sumber_daya": form
        }
        return render(request, 'create_permohonan_sumber_daya.html', context)

def create_permohonan_sumber_daya(request, no_transaksi):
    username = request.session['username']
    if request.session['role'] != 'Petugas Faskes':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        if request.method == "POST":
            no_transaksi = request.POST.get('no_transaksi')

            total_item = request.POST.get('total_item')
            daftar_item = []
            for i in range(int(total_item)):
                daftar_item.append(request.POST.getlist('list_daftar_item[' + str(i) + '][]'))

            insert_item_query = 'INSERT INTO DAFTAR_ITEM values'
            no_urut = 1
            for item in daftar_item:
                insert_item_query += f"({no_transaksi}, {no_urut}, {item[0]}, '{item[1]}'),"
                no_urut+=1
            insert_item_query = insert_item_query[:-1]
            
            catatan = request.POST.get('catatan')

            cursor.execute(insert_item_query)
            cursor.execute(f"INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES VALUES('{no_transaksi}','{username}','{catatan}')")

            data = {
                'redirecturl' : "/readPermohonanSumberDaya/"
            }

            return JsonResponse(data)

        cursor.execute('SELECT kode, nama FROM ITEM_SUMBER_DAYA')
        tipe_item = list(cursor.fetchall())
        context = {
            "daftar_item": DaftarItem(),
            "no_transaksi": no_transaksi,
            "tipe_item": tipe_item 
        }
        return render(request, 'create_daftar_item.html', context)

def read_permohonan_sumber_daya(request):
    username = request.session['username']
    role = request.session['role']
    if (role != 'Petugas Faskes') and (role != 'Admin Satgas'):
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        query_filter = ""
        if role == 'Petugas Faskes':
            query_filter += f"AND PN.username='{username}' "

        cursor.execute("SELECT r.nomor_permohonan, pn.nama, MAX(r.tanggal), total_berat, total_item, catatan," + 
                        "RIGHT(string_agg(kode_status_permohonan, ' '),3) " + 
                        'FROM permohonan_with_username PN, transaksi_sumber_daya T, riwayat_status_permohonan R ' +
                        "WHERE R.nomor_permohonan = PN.no_transaksi_sumber_daya AND PN.no_transaksi_sumber_daya = T.nomor " + query_filter +
                        'GROUP BY (r.nomor_permohonan, pn.nama, total_berat, total_item, catatan) ' +
                        'ORDER BY r.nomor_permohonan')
        data_fetch = list(cursor.fetchall())
        
        list_permohonan = []
        for data in data_fetch:
            list_data = list(data)
            list_data[3] = list_data[3]/1000
            list_permohonan.append(list_data)

        context = {
            'data_permohonan' : list_permohonan,
            'role' : role
        }
        return render(request, 'read_permohonan_sumber_daya.html', context)

def delete_permohonan_sumber_daya(request, no_transaksi):
    if request.session['role'] != 'Petugas Faskes':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM TRANSAKSI_SUMBER_DAYA WHERE nomor=' + str(no_transaksi))
        messages.info(request, "Permohonan berhasil dihapus.")
        return redirect('alishayb:read_permohonan_sumber_daya')

def detail_permohonan_sumber_daya(request, no_transaksi):
    role = request.session['role']
    if (role != 'Petugas Faskes') and (role != 'Admin Satgas'):
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        cursor.execute('SELECT DISTINCT nomor, PE.nama, tanggal, no_urut, DI.nama, jumlah_item, catatan ' +
                        'FROM (permohonan_sumber_daya_faskes ps JOIN pengguna p ON ps.username_petugas_faskes = p.username) PE, ' +
                        '(daftar_item d join item_sumber_daya i on d.kode_item_sumber_daya = i.kode) DI, '+
                        'transaksi_sumber_daya T ' +
                        'WHERE PE.no_transaksi_sumber_daya = T.nomor AND DI.no_transaksi_sumber_daya = T.nomor AND T.nomor=' + str(no_transaksi))
        context = {
            "data_permohonan" : cursor.fetchall()
        }
        return render(request, 'detail_permohonan_sumber_daya.html', context)

def update_permohonan_sumber_daya(request, no_transaksi):
    username = request.session['username']
    if request.session['role'] != 'Petugas Faskes':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        if request.method == "POST":
            no_transaksi = request.POST.get('no_transaksi')

            total_item = request.POST.get('total_item')[0]
            daftar_item = []
            for i in range(int(total_item)):
                daftar_item.append(request.POST.getlist('list_daftar_item[' + str(i) + '][]'))

            cursor.execute(f"DELETE FROM DAFTAR_ITEM WHERE no_transaksi_sumber_daya='{no_transaksi}'")

            insert_item_query = 'INSERT INTO DAFTAR_ITEM values'
            no_urut = 1
            for item in daftar_item:
                insert_item_query += f"({no_transaksi}, {no_urut}, {item[1]}, '{item[0]}'),"
                no_urut+=1
            insert_item_query = insert_item_query[:-1]
            
            catatan = request.POST.get('catatan')

            cursor.execute(insert_item_query)
            cursor.execute(f"DELETE FROM permohonan_sumber_daya_faskes WHERE no_transaksi_sumber_daya='{no_transaksi}'")
            cursor.execute(f"INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES VALUES('{no_transaksi}','{username}','{catatan}')")

            data = {
                'redirecturl' : "/readPermohonanSumberDaya/"
            }

            return JsonResponse(data)
            
        cursor.execute('SELECT P.nama, D.no_urut, D.kode_item_sumber_daya, D.jumlah_item, PS.catatan ' +
                        'FROM PERMOHONAN_SUMBER_DAYA_FASKES PS, DAFTAR_ITEM D, PENGGUNA P ' +
                        'WHERE PS.no_transaksi_sumber_daya = D.no_transaksi_sumber_daya AND ' +
                        'P.username = PS.username_petugas_faskes AND PS.no_transaksi_sumber_daya=' + str(no_transaksi))
        data_permohonan = list(cursor.fetchall())
        data_permohonan_json = json.dumps(data_permohonan)
        
        cursor.execute('SELECT kode, nama FROM ITEM_SUMBER_DAYA')
        tipe_item = list(cursor.fetchall())

        context = {
            "no_transaksi": no_transaksi,
            "tipe_item": tipe_item,
            "data_permohonan": data_permohonan,
            "data_permohonan_json": data_permohonan_json
        }
        return render(request, 'update_daftar_item.html', context)

def proses_permohonan(request, no_transaksi):
    username = request.session['username']
    if request.session['role'] != 'Admin Satgas':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        now = date.today().strftime("%Y-%m-%d")
        cursor.execute(f"INSERT INTO RIWAYAT_STATUS_PERMOHONAN values('PRO',{no_transaksi},'{username}','{now}')")
        return redirect('alishayb:read_permohonan_sumber_daya')

def tolak_permohonan(request, no_transaksi):
    username = request.session['username']
    if request.session['role'] != 'Admin Satgas':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        now = date.today().strftime("%Y-%m-%d")
        cursor.execute(f"INSERT INTO RIWAYAT_STATUS_PERMOHONAN values('REJ',{no_transaksi},'{username}','{now}')")
        return redirect('alishayb:read_permohonan_sumber_daya')

def selesai_permohonan_dengan_masalah(request, no_transaksi):
    username = request.session['username']
    if request.session['role'] != 'Admin Satgas':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        now = date.today().strftime("%Y-%m-%d")
        cursor.execute(f"INSERT INTO RIWAYAT_STATUS_PERMOHONAN values('MAS',{no_transaksi},'{username}','{now}')")
        return redirect('alishayb:read_permohonan_sumber_daya')

def selesai_permohonan(request, no_transaksi):
    username = request.session['username']
    if request.session['role'] != 'Admin Satgas':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        now = date.today().strftime("%Y-%m-%d")
        cursor.execute(f"INSERT INTO RIWAYAT_STATUS_PERMOHONAN values('FIN',{no_transaksi},'{username}','{now}')")
        return redirect('alishayb:read_permohonan_sumber_daya')

def riwayat_status_permohonan(request, no_transaksi):
    role = request.session['role']
    if (role != 'Petugas Faskes') and (role != 'Admin Satgas'):
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        cursor.execute("SELECT r.kode_status_permohonan, s.nama, r.username_admin, r.tanggal " +
                        "FROM riwayat_status_permohonan r JOIN status_permohonan s ON r.kode_status_permohonan = s.kode " +
                        f"WHERE r.nomor_permohonan={no_transaksi}")
        list_riwayat = list(cursor.fetchall())
        list_riwayat.reverse()
        
        context = {
            "no_transaksi": no_transaksi,
            "list_riwayat": list_riwayat,
        }

        return render(request, 'riwayat_status_permohonan.html', context)

def create_item_sumber_daya(request):
    if request.session['role'] != 'Supplier':
        return redirect('user:welcome')
    username = request.session['username']
    with connection.cursor() as cursor:
        if request.method == "POST":
            form = itemSumberDayaForm(request.POST)

            if form.is_valid():
                nama_item = form.cleaned_data.get('nama_item')
                harga_satuan = form.cleaned_data.get('harga_satuan')
                berat_satuan = form.cleaned_data.get('berat_satuan')
                kode_tipe_item = form.cleaned_data.get('tipe_item')
                
                cursor.execute(f"SELECT RIGHT(kode, 2) FROM item_sumber_daya WHERE kode_tipe_item='{kode_tipe_item}' ORDER BY kode DESC")
                new_kode = int(cursor.fetchone()[0]) + 1

                insert_kode = ""
                if(new_kode < 10):
                    insert_kode += "0"
                insert_kode += str(new_kode)

                cursor.execute(f"INSERT INTO item_sumber_daya values('{kode_tipe_item}{insert_kode}', '{username}', '{nama_item}', {harga_satuan}, {berat_satuan}, '{kode_tipe_item}')")
                
                cursor.execute(f"SELECT nama FROM tipe_item WHERE kode='{kode_tipe_item}'")
                nama_tipe_item = cursor.fetchone()[0]

                context = {
                    'message' : 'Item berhasil dibuat!',
                    'kode_item' : kode_tipe_item + insert_kode,
                    'supplier' : username,
                    'nama_item' : nama_item,
                    'harga_satuan' : harga_satuan,
                    'berat_satuan' : berat_satuan,
                    'kode_tipe_item' : kode_tipe_item,
                    'tipe_item' : nama_tipe_item

                }

                return render(request, 'detail_create_item_sumber_daya.html', context)
        
        form = itemSumberDayaForm()
        form.fields['supplier'].widget.attrs.update({'disabled':'disabled', 'value': username})
        form.fields['berat_satuan'].widget.attrs.update({'placeholder':'Gram'})
        context = {
            'create_item_form': form
        }
        return render(request, 'create_item_sumber_daya.html', context)

def read_item_sumber_daya(request):
    role = request.session['role']
    if (role != 'Supplier') and (role != 'Admin Satgas'):
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        cursor.execute("SELECT I.kode, S.nama_organisasi, I.nama, I.harga_satuan, I.berat_satuan, T.nama, I.username_supplier " + 
                        "FROM ITEM_SUMBER_DAYA I, TIPE_ITEM T, SUPPLIER S " +
                        "WHERE S.username = I.username_supplier AND I.kode_tipe_item = T.kode")
        data_fetch = list(cursor.fetchall())

        list_item = []
        for data in data_fetch:
            list_data = list(data)
            list_data[4] = list_data[4]/1000
            list_item.append(list_data)

        username = request.session['username']
        context = {
            'list_item': list_item,
            'supplier': username
        }
        
        return render(request, 'read_item_sumber_daya.html', context)

def update_item_sumber_daya(request, kode_item):
    if request.session['role'] != 'Supplier':
        return redirect('user:welcome')
        
    with connection.cursor() as cursor:
        username = request.session['username']
        if(request.method == "POST"):
            nama_item = request.POST.get('nama_item')
            harga_satuan = request.POST.get('harga_satuan')
            berat_satuan = request.POST.get('berat_satuan')
            tipe_item = request.POST.get('tipe_item')
            print(tipe_item)

            cursor.execute(f"DELETE FROM item_sumber_daya WHERE kode='{kode_item}'")

            cursor.execute(f"SELECT RIGHT(kode, 2) FROM item_sumber_daya WHERE kode_tipe_item='{tipe_item}' ORDER BY kode DESC")
            new_kode = int(cursor.fetchone()[0]) + 1

            insert_kode = ""
            if(new_kode < 10):
                insert_kode += "0"
            insert_kode += str(new_kode)

            cursor.execute(f"INSERT INTO item_sumber_daya values('{tipe_item}{insert_kode}', '{username}', '{nama_item}', {harga_satuan}, {berat_satuan}, '{tipe_item}')")

            return redirect('alishayb:read_item_sumber_daya')
        
        cursor.execute(f"SELECT * FROM item_sumber_daya WHERE kode='{kode_item}'")
        item_data = cursor.fetchone()

        cursor.execute("SELECT * FROM tipe_item")
        tipe_item = list(cursor.fetchall())
        
        context = {
            'kode_item' : kode_item,
            'supplier' : username,
            'nama_item' : item_data[2],
            'harga_satuan' : item_data[3],
            'berat_satuan' : item_data[4],
            'tipe_item' : tipe_item
        }
            
        return render(request, 'update_item_sumber_daya.html', context)

def delete_item_sumber_daya(request, kode_item):
    if request.session['role'] != 'Supplier':
        return redirect('user:welcome')
    with connection.cursor() as cursor:
        cursor.execute(f"DELETE FROM ITEM_SUMBER_DAYA WHERE kode='{kode_item}'")
        messages.info(request, "Item berhasil dihapus!")

        return redirect('alishayb:read_item_sumber_daya')
