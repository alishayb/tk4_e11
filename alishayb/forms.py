from django import forms
from django.db import connection

class TransaksiSumberDaya(forms.Form):
    nomor_transaksi = forms.IntegerField(widget=forms.TextInput(attrs={'disabled':'disabled'}))
    username_petugas = forms.CharField(widget=forms.TextInput(attrs={'disabled':'disabled'}))

TIPE_ITEM = [
    ('APD', 'Alat Pelindung Diri'),
    ('VEN', 'Ventilator'),
    ('BED', 'Tempat Tidur'),
    ('MSK', 'Masker'),
    ('VAK', 'Vaksin'),
    ('PCR', 'PCR Test Kit'),
    ('RAP', 'Rapid Test Kit')
]

class DaftarItem(forms.Form):
    no_urut = forms.IntegerField(widget=forms.TextInput(attrs={'disabled':'disabled'}))
    item = forms.ChoiceField(choices = TIPE_ITEM)
    jumlah_item = forms.IntegerField() 

class itemSumberDayaForm(forms.Form):
    supplier = forms.CharField(required=False)
    nama_item = forms.CharField(required=True)
    harga_satuan = forms.IntegerField(required=True)
    berat_satuan = forms.IntegerField(required=True)
    tipe_item = forms.ChoiceField(choices = TIPE_ITEM)
    

    