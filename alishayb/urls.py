from django.urls import path
from . import views

app_name = 'alishayb'

urlpatterns = [
    path('createPermohonanSumberDaya/', views.create_transaksi_sumber_daya, name="create_transaksi_sumber_daya"),
    path('createDaftarItem/<int:no_transaksi>/', views.create_permohonan_sumber_daya, name="create_permohonan_sumber_daya"),
    path('readPermohonanSumberDaya/', views.read_permohonan_sumber_daya, name="read_permohonan_sumber_daya"),
    path('deletePermohonanSumberDaya/<int:no_transaksi>/', views.delete_permohonan_sumber_daya, name="delete_permohonan_sumber_daya"),
    path('detailPermohonanSumberDaya/<int:no_transaksi>/', views.detail_permohonan_sumber_daya, name="detail_permohonan_sumber_daya"),
    path('updatePermohonanSumberDaya/<int:no_transaksi>/', views.update_permohonan_sumber_daya, name="update_permohonan_sumber_daya"),

    # change status
    path('prosesPermohonan/<int:no_transaksi>/', views.proses_permohonan, name="proses_permohonan"),
    path('tolakPermohonan/<int:no_transaksi>/', views.tolak_permohonan, name="reject_permohonan"),
    path('selesaiPermohonanDenganMasalah/<int:no_transaksi>/', views.selesai_permohonan_dengan_masalah, name="selesai_permohonan_dengan_masalah"),
    path('selesaiPermohonan/<int:no_transaksi>/', views.selesai_permohonan, name="selesai_permohonan"),

    # riwayat status
    path('riwayatStatusPermohonan/<int:no_transaksi>/', views.riwayat_status_permohonan, name="riwayat_status_permohonan"),

    path('createItemSumberDaya/', views.create_item_sumber_daya, name="create_item_sumber_daya"),
    path('readItemSumberDaya/', views.read_item_sumber_daya, name="read_item_sumber_daya"),
    path('updateItemSumberDaya/<str:kode_item>/', views.update_item_sumber_daya, name="update_item_sumber_daya"),
    path('deleteItemSumberDaya/<str:kode_item>/', views.delete_item_sumber_daya, name="delete_item_sumber_daya"),
]