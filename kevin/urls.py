from django.urls import path
from . import views

app_name = 'kevin'

urlpatterns = [
    path('createKendaraan/', views.createKendaraan, name='createKendaraan'),
    path('readKendaraan/', views.readKendaraan, name='readKendaraan'),
    path('deleteKendaraan/<slug:delete_id>/',views.deleteKendaraan, name='deleteKendaraan'),
    path('updateKendaraan/<slug:update_id>/',views.updateKendaraan, name='updateKendaraan'),

    path('createLokasi/', views.createLokasi, name='createLokasi'),
    path('readLokasi/', views.readLokasi, name='readLokasi'),
    path('deleteLokasi/<slug:delete_id>/',views.deleteLokasi, name='deleteLokasi'),
    path('updateLokasi/<slug:update_id>/',views.updateLokasi, name='updateLokasi'),

    path('createBatchPengiriman/', views.createBatchPengiriman, name='createBatchPengiriman'),
]
