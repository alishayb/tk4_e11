from django import forms

PROVINSI= [
    ('Aceh', 'Aceh'),
    ('Sumatra Utara', 'Sumatra Utara'),
    ('Sumatra Barat', 'Sumatra Barat'),
    ('Riau', 'Riau'),
    ('Kepulauan Riau', 'Kepulauan Riau'),
    ('Jambi', 'Jambi'),
    ('Bengkulu', 'Bengkulu'),
    ('Sumatra Selatan', 'Sumatra Selatan'),
    ('Kepulauan Bangka Belitung', 'Kepulauan Bangka Belitung'),
    ('Lampung', 'Lampung'),
    ('Banten', 'Banten'),
    ('Jawa Barat', 'Jawa Barat'),
    ('DKI Jakarta', 'DKI Jakarta'),
    ('Jawa Tengah', 'Jawa Tengah'),
    ('Daerah Istimewa Yogyakarta', 'Daerah Istimewa Yogyakarta'),
    ('Jawa Timur', 'Jawa Timur'),
    ('Bali', 'Bali'),
    ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
    ('Kalimantan Barat', 'Kalimantan Barat'),
    ('Kalimantan Selatan', 'Kalimantan Selatan'),
    ('Kalimantan Tengah', 'Kalimantan Tengah'),
    ('Kalimantan Timur', 'Kalimantan Timur'),
    ('Kalimantan Utara', 'Kalimantan Utara'),
    ('Gorontalo', 'Gorontalo'),
    ('Sulawesi Barat', 'Sulawesi Barat'),
    ('Sulawesi Selatan', 'Sulawesi Selatan'),
    ('Sulawesi Tengah', 'Sulawesi Tengah'),
    ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
    ('Sulawesi Utara', 'Sulawesi Utara'),
    ('Maluku', 'Maluku'),
    ('Maluku Utara', 'Maluku Utara'),
    ('Papua Barat', 'Papua Barat'),
    ('Papua', 'Papua')
    ]

class formRegistKendaraan(forms.Form):
    nomor = forms.CharField(required=True)
    nama = forms.CharField(required=True)
    jenis_kendaraan = forms.CharField(required=True)
    berat_maksimum = forms.IntegerField(required=True)


class formRegistLokasi(forms.Form):
    lokasi = forms.CharField(required=True)
    provinsi = forms.ChoiceField(choices = PROVINSI)
    kabupaten_kota = forms.CharField(required=True)
    kecamatan = forms.CharField(required=True)
    keluarahan =  forms.CharField(required=True)
    jalan = forms.CharField(required=True)



