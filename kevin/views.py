from django.shortcuts import redirect, render
from django.db import connection
from kevin.forms import *
from django.contrib import messages

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def createKendaraan(request):
    message =""
    if request.method=="POST" :
        form = formRegistKendaraan(request.POST)
        if form.is_valid():
            nomor = form.cleaned_data.get('nomor')
            nama = form.cleaned_data.get('nama')
            jenis_kendaraan = form.cleaned_data.get('jenis_kendaraan')
            berat_maksimum = form.cleaned_data.get('berat_maksimum')

            with connection.cursor() as cursor :
                try :
                    cursor.execute('INSERT INTO KENDARAAN values(%s,%s,%s,%s)',[nomor,nama,jenis_kendaraan,berat_maksimum])   
                    message = "Kendaraan berhasil terdaftar"      
                except :
                    message = "Kendaraan yang anda masukan sudah terdaftar"   
        
        return redirect('kevin:readKendaraan')

    regist_kendaraan = formRegistKendaraan()

    context = {
			"create_kendaraan":regist_kendaraan, "message":message
		}

    return render(request, 'createKendaraan.html', context)


def readKendaraan(request):
    with connection.cursor() as cursor:
        admin = 'admin_1@gmail.com'
        supplier = 'supplier_1@gmail.com'
        username_pengguna = admin
        cursor.execute('SELECT username FROM ADMIN_SATGAS WHERE username =%s',[username_pengguna])
        isboolAdmin = cursor.fetchall()
        cursor.execute('SELECT username FROM SUPPLIER WHERE username =%s',[username_pengguna])
        isboolSupplier = cursor.fetchall()

        if(len(isboolAdmin)):
            cursor.execute('SELECT * FROM KENDARAAN k')
            role = 1

        daftar = cursor.fetchall()
        print(daftar)
        context ={'daftar':daftar, 'role':role}
        row = dictfetchall(cursor)
        return render(request,'readKendaraan.html',context)

def deleteKendaraan(request, delete_id):
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM KENDARAAN WHERE nomor = %s',[delete_id])
        return redirect('kevin:readKendaraan')

def updateKendaraan(request, update_id):
    message=""
    with connection.cursor() as cursor:
        if request.method == "POST" :
            form = formRegistKendaraan(request.POST)
            if form.is_valid():
                nomor = form.cleaned_data.get('nomor')
                nama = form.cleaned_data.get('nama')
                jenis_kendaraan = form.cleaned_data.get('jenis_kendaraan')
                berat_maksimum = form.cleaned_data.get('berat_maksimum')

                delete_kendaraan = "DELETE FROM KENDARAAN WHERE nomor =%s "
                tuple_query = [update_id]
                cursor.execute(delete_kendaraan, tuple_query)

                insert_kendaraan = 'INSERT INTO KENDARAAN VALUES(%s,%s,%s,%s)'
                tuple_query = [nomor,nama,jenis_kendaraan,berat_maksimum]
                cursor.execute(insert_kendaraan,tuple_query)

                message = "Kendaraan telah di-update"
            
        regist_kendaraan = formRegistKendaraan()

        context = {
                "update_kendaraan":regist_kendaraan , 
                "update_id" : update_id,
                "message" : message
            }

        return render(request, 'updateKendaraan.html', context )

def createLokasi(request):
    message =""
    if request.method=="POST" :
        form = formRegistLokasi(request.POST)
        if form.is_valid():
            lokasi = form.cleaned_data.get('lokasi')
            provinsi = form.cleaned_data.get('provinsi')
            kabupaten_kota = form.cleaned_data.get('kabupaten_kota')
            kecamatan = form.cleaned_data.get('kecamatan')
            keluarahan =  form.cleaned_data.get('keluarahan')
            jalan = form.cleaned_data.get('jalan')

            with connection.cursor() as cursor :
                try :
                    cursor.execute('INSERT INTO LOKASI values(%s,%s,%s,%s,%s,%s)',[lokasi,provinsi,kabupaten_kota,kecamatan,keluarahan,jalan])   
                    message = "Lokasi berhasil terdaftar"      
                except :
                    message = "Lokasi yang anda masukan sudah terdaftar"   
                    

    regist_lokasi = formRegistLokasi()

    context = {
			"create_lokasi":regist_lokasi, "message":message
		}

    return render(request, 'createLokasi.html', context)

def readLokasi(request):
    with connection.cursor() as cursor:
        admin = 'admin_1@gmail.com'
        supplier = 'supplier_1@gmail.com'
        username_pengguna = admin
        cursor.execute('SELECT username FROM ADMIN_SATGAS WHERE username =%s',[username_pengguna])
        isboolAdmin = cursor.fetchall()
        cursor.execute('SELECT username FROM SUPPLIER WHERE username =%s',[username_pengguna])
        isboolSupplier = cursor.fetchall()

        if(len(isboolAdmin)):
            cursor.execute('SELECT * FROM LOKASI')
            role = 1
       
        daftar = cursor.fetchall()
        print(daftar)
        context ={'daftar':daftar, 'role':role}
        row = dictfetchall(cursor)
        return render(request,'readLokasi.html',context)

def deleteLokasi(request, delete_id):
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM LOKASI WHERE Id = %s',[delete_id])
        return redirect('kevin:readLokasi')

def updateLokasi(request, update_id):
    message=""
    with connection.cursor() as cursor:
        if request.method == "POST" :
            form = formRegistLokasi(request.POST)
            if form.is_valid():
                lokasi = form.cleaned_data.get('lokasi')
                provinsi = form.cleaned_data.get('provinsi')
                kabupaten_kota = form.cleaned_data.get('kabupaten_kota')
                kecamatan = form.cleaned_data.get('kecamatan')
                keluarahan =  form.cleaned_data.get('keluarahan')
                jalan = form.cleaned_data.get('jalan')

                delete_lokasi = "DELETE FROM LOKASI WHERE id =%s "
                tuple_query = [update_id]
                cursor.execute(delete_lokasi, tuple_query)

                insert_lokasi = 'INSERT INTO LOKASI values(%s,%s,%s,%s,%s,%s)'
                tuple_query = [lokasi,provinsi,kabupaten_kota,kecamatan,keluarahan,jalan]   
                cursor.execute(insert_lokasi,tuple_query)
                message = "Lokasi telah diupdate" 
            
        regist_lokasi = formRegistLokasi()

        context = {
                "update_lokasi":regist_lokasi, 
                "update_id" : update_id,
                "message" : message
            }

        return render(request, 'updateLokasi.html', context )


def createBatchPengiriman(request):
    with connection.cursor() as cursor:
        admin = 'admin_1@gmail.com'
        supplier = 'supplier_1@gmail.com'
        username_pengguna = admin
        cursor.execute('SELECT username FROM ADMIN_SATGAS WHERE username =%s',[username_pengguna])
        isboolAdmin = cursor.fetchall()
        cursor.execute('SELECT username FROM SUPPLIER WHERE username =%s',[username_pengguna])
        isboolSupplier = cursor.fetchall()

        if(len(isboolAdmin)):
            cursor.execute('SELECT R.Nomor_permohonan , P.Username_petugas_faskes , F.Id_lokasi , S.Total_berat , R.Kode_status_permohonan , I.Kode_status_batch_pengiriman  FROM RIWAYAT_STATUS_PERMOHONAN R  '+
                            'LEFT JOIN PERMOHONAN_SUMBER_DAYA_FASKES P '+
                            'ON R.Nomor_permohonan = P.No_transaksi_sumber_daya ' +
                            'LEFT JOIN TRANSAKSI_SUMBER_DAYA S '+
                            'ON S.Nomor = P.No_transaksi_sumber_daya ' +
                            'LEFT JOIN FASKES F '+
                            'ON P.Username_petugas_faskes = F.Username_petugas '+
                            'LEFT JOIN BATCH_PENGIRIMAN B '+
                            'ON B.nomor_transaksi_sumber_daya = S.Nomor ' +
                            'LEFT JOIN RIWAYAT_STATUS_PENGIRIMAN I '+
                            'ON B.kode = I.Kode_batch ' )
            role = 1
       
        daftar = cursor.fetchall()
        print(daftar)
        context ={'daftar':daftar, 'role':role}
        row = dictfetchall(cursor)
        return render(request,'createBatchPengiriman.html',context)