from django.urls import path
from . import views

app_name = "dodo"

urlpatterns = [
    path('get-lokasi-alamat/<slug:id_lokasi>', views.locationIDToAddressEndpoint, name="locationIDToAddressEndpoint"),
    path('get-faskes-tipe/<slug:id_tipe>', views.faskesCodeToTypeNameEndpoint, name="faskesCodeToTypeNameEndpoint"),
    path('list-faskes/', views.readFaskes, name="readFaskes"),
    path('delete-faskes/<slug:kode_faskes>', views.deleteFaskes, name="deleteFaskes"),
    path('update-faskes/<slug:kode_faskes>', views.updateFaskes, name="updateFaskes"),
    path('create-faskes/', views.createFaskes, name="createFaskes")
]