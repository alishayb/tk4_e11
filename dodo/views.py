from django.shortcuts import render, redirect
from django.db import connection
from django.http import JsonResponse, HttpResponse
from django.contrib import messages
# this is a bad idea, but
g_debug = False

# number 12
def createFaskes(request):
    if not (authenticateAdmin(request.session)):
        return HttpResponse("Forbidden", status = 403)

    with connection.cursor() as cursor:
        if request.method == "POST":
            id_lokasi = request.POST.get("id_lokasi")
            kode_faskes_nasional = request.POST.get("kode_faskes_nasional")
            username_petugas = request.POST.get("username_petugas")
            kode_tipe_faskes = request.POST.get("kode_tipe_faskes")
            if not (checkNoneAreEmpty(id_lokasi, kode_faskes_nasional, username_petugas, kode_tipe_faskes)):
                return HttpResponse("Bad request", status = 400)
            cursor.execute(f"insert into faskes(id_lokasi, kode_faskes_nasional, username_petugas, kode_tipe_faskes) values ('{id_lokasi}', '{kode_faskes_nasional}', '{username_petugas}', '{kode_tipe_faskes}')")
            return redirect("dodo:readFaskes")
        else:
            cursor.execute("select username from petugas_faskes where username not in (select username_petugas from faskes)")
            list_of_petugas = cursor.fetchall()
            cursor.execute("select id_lokasi from faskes")
            list_of_id_lokasi = cursor.fetchall()
            cursor.execute("select kode from tipe_faskes")
            list_of_kode_tipe_faskes = cursor.fetchall()
            context = {
                'list_of_petugas': [i[0] for i in list_of_petugas],
                'list_of_id_lokasi': [i[0] for i in list_of_id_lokasi],
                'list_of_kode_tipe_faskes': [i[0] for i in list_of_kode_tipe_faskes]
                }
            return render(request, 'create_faskes.html', context)

def readFaskes(request):
    # impossible, but just in case, this is to check if the admin is not also a supplier
    isAdmin = False
    if not (authenticateAdmin(request.session)):
        if not (authenticateSupplier(request.session)):
            return HttpResponse("Forbidden", status = 403)
    else:
        isAdmin = True
    
    list_of_faskes = {}
    with connection.cursor() as cursor:
        cursor.execute("select "
        "kode_faskes_nasional, "
        "concat_ws(', ', jalan_no, keluraham, kecamatan, kabkot, provinsi) as alamat, "
        "nama, "
        "nama_tipe "
        "from faskes, lokasi, tipe_faskes, pengguna "
        "where faskes.kode_tipe_faskes = tipe_faskes.kode "
        "and faskes.username_petugas = pengguna.username "
        "and faskes.id_lokasi = lokasi.id"
        )
        list_of_faskes = dictfetchall(cursor)
    context = {'list_of_faskes': list_of_faskes}
    if (isAdmin):
        return render(request, 'read_faskes_for_admin.html', context)
    else:
        return render(request, 'read_faskes_for_supplier.html', context)
    
def updateFaskes(request, kode_faskes):
    if not (authenticateAdmin(request.session)):
        return HttpResponse("Forbidden", status = 403)

    with connection.cursor() as cursor:
        if request.method == "POST":
            id_lokasi = request.POST.get("id_lokasi")
            kode_faskes_nasional = kode_faskes
            username_petugas = request.POST.get("username_petugas")
            kode_tipe_faskes = request.POST.get("kode_tipe_faskes")
            if not (checkNoneAreEmpty(id_lokasi, kode_faskes_nasional, username_petugas, kode_tipe_faskes)):
                return HttpResponse("Bad request", status = 400)
            cursor.execute(f"update faskes "
            f"set id_lokasi = '{id_lokasi}', "
            f"kode_tipe_faskes = '{kode_tipe_faskes}', "
            f"username_petugas = '{username_petugas}' "
            f"where kode_faskes_nasional = '{kode_faskes_nasional}'")
            return redirect("dodo:readFaskes")
        else:
            cursor.execute("select username from petugas_faskes where username not in (select username_petugas from faskes)")
            list_of_petugas = cursor.fetchall()
            cursor.execute(f"select * from faskes where kode_faskes_nasional = '{kode_faskes}'")
            row = dictfetchall(cursor)[0]
            cursor.execute("select id_lokasi from faskes")
            list_of_id_lokasi = cursor.fetchall()
            cursor.execute("select kode from tipe_faskes")
            list_of_kode_tipe_faskes = cursor.fetchall()            
            context = {
                'list_of_petugas': [i[0] for i in list_of_petugas],
                'current_id_lokasi': row['id_lokasi'],
                'current_username_petugas': row['username_petugas'],
                'current_kode_tipe_faskes': row['kode_tipe_faskes'],
                'current_kode_faskes_nasional': kode_faskes,
                'list_of_petugas': [i[0] for i in list_of_petugas],
                'list_of_id_lokasi': [i[0] for i in list_of_id_lokasi],
                'list_of_kode_tipe_faskes': [i[0] for i in list_of_kode_tipe_faskes]
            }
            return render(request, 'update_faskes.html', context)
    
def deleteFaskes(request, kode_faskes):
    if not (authenticateAdmin(request.session)):
        return HttpResponse("Forbidden", status = 403)

    with connection.cursor() as cursor:
        cursor.execute(f"delete from faskes where kode_faskes_nasional = '{kode_faskes}'")
        messages.success(request, "Data berhasil dihapus")
        return redirect("dodo:readFaskes")

# number 13

# number 14

# utils

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def authenticateAdmin(session_data) -> bool:
    return authenticateUser(session_data, "Admin Satgas")
    
def authenticateSupplier(session_data) -> bool:
    return authenticateUser(session_data, "Supplier")

def checkNoneAreEmpty(*fields) -> bool:
    for field in fields:
        if field is None or len(field) == 0:
            return False
    return True

def authenticateUser(session_data, role: str) -> bool:
    try:
        if not g_debug:
            return session_data['role'] == role
        else:
            return True
    except:
        return False
        
def locationIDToAddressEndpoint(request, id_lokasi):
    if request.method == "GET":
        with connection.cursor() as cursor:
            cursor.execute(
            f"select concat_ws(', ', jalan_no, keluraham, kecamatan, kabkot, provinsi) as alamat from lokasi where id = '{id_lokasi}'"
            )
            address_data = cursor.fetchone()
        if len(address_data) == 0:
            address_data = ['None']
    return JsonResponse({'address': address_data[0]})

def faskesCodeToTypeNameEndpoint(request, id_tipe):
    if request.method == "GET":
        with connection.cursor() as cursor:
            cursor.execute(
            f"select nama_tipe from tipe_faskes where kode = '{id_tipe}'"
            )
            type_data = cursor.fetchone()
        if len(type_data) == 0:
            address_data = ['None']
    return JsonResponse({'faskes': type_data[0]})