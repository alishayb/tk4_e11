from django.apps import AppConfig


class DodoConfig(AppConfig):
    name = 'dodo'
