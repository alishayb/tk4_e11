from django.shortcuts import redirect, render
from django.db import connection
from user.forms import *
from django.db.utils import InternalError
from django.contrib import messages

# Create your views here.

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def login(request):
    if request.method == 'POST':
        form = formLogin(request.POST)
        if form.is_valid():     
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            with connection.cursor() as cursor:
                cursor.execute(f"SELECT username FROM pengguna WHERE username='{username}' AND password='{password}'")
                user_data = cursor.fetchone()

                if user_data is not None:
                    request.session['username'] = username
                    request.session['role'] = getRole(username)

                    return redirect('user:dashboard')
                else:
                    form = formLogin()
                    context = {
                        'login_form' : form,
                        'message' : 'Incorrect username or password. Please try again.'
                    }
                    return render(request, 'login.html', context)

    form = formLogin()
    context = {
        'login_form' : form
    }
    return render(request, 'login.html', context)

def getRole(username):
    with connection.cursor() as cursor:
        cursor.execute(f"SELECT username FROM admin_satgas WHERE username='{username}'")
        role = cursor.fetchone()

        if role is not None:
            return 'Admin Satgas'

        cursor.execute(f"SELECT username FROM petugas_faskes WHERE username='{username}'")
        role = cursor.fetchone()

        if role is not None:
            return 'Petugas Faskes'
        
        cursor.execute(f"SELECT username FROM supplier WHERE username='{username}'")
        role = cursor.fetchone()

        if role is not None:
            return 'Supplier'

        cursor.execute(f"SELECT username FROM petugas_distribusi WHERE username='{username}'")
        role = cursor.fetchone()

        if role is not None:
            return 'Petugas Distribusi'

    return 'Unknown'

def logout(request):
    request.session.flush()
    context = {
        'message' : 'Logout berhasil.'
    }
    return render(request, 'welcome.html', context)

def register(request):
    return render(request, 'register.html')

def welcome(request):
    return render(request, 'welcome.html')

def dashboard(request):
    role = request.session['role']
    username = request.session['username']
    context = {'role':role,'username':username}
    print(role)
    return render(request,'dashboard.html',context)

def daftar_admin_satgas(request):
    if request.method=="POST" :
        form = formRegistAdminSatgas(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            alamat_kelurahan = form.cleaned_data.get('alamat_kelurahan')
            alamat_kecamatan = form.cleaned_data.get('alamat_kecamatan')
            alamat_kota = form.cleaned_data.get('alamat_kota')
            provinsi = form.cleaned_data.get('provinsi')
            no_telp = form.cleaned_data.get('no_telp')

            with connection.cursor() as cursor:
                try:
                    cursor.execute('INSERT INTO PENGGUNA values(%s,%s,%s,%s,%s,%s,%s,%s)',[username,password,nama_lengkap,alamat_kelurahan,alamat_kecamatan,alamat_kota,provinsi,no_telp])  
                    cursor.execute('INSERT INTO ADMIN_SATGAS values(%s)',[username])
                    
                    request.session['username']=username
                    request.session['role'] = 'Admin Satgas'
                    return redirect('user:dashboard')
                except(InternalError):
                    messages.error(request, "Username sudah terdaftar!")
                    return redirect('user:daftar_admin_satgas')

    context = {
        "create_admin_satgas": formRegistAdminSatgas()
    }
    return render(request, 'daftar_admin_satgas.html', context)

def daftar_petugas_faskes(request):
    if request.method=="POST" :
        form = formRegistPetugasFaskes(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            alamat_kelurahan = form.cleaned_data.get('alamat_kelurahan')
            alamat_kecamatan = form.cleaned_data.get('alamat_kecamatan')
            alamat_kota = form.cleaned_data.get('alamat_kota')
            provinsi = form.cleaned_data.get('provinsi')
            no_telp = form.cleaned_data.get('no_telp')

            with connection.cursor() as cursor:
                try:
                    cursor.execute('INSERT INTO PENGGUNA values(%s,%s,%s,%s,%s,%s,%s,%s)',[username,password,nama_lengkap,alamat_kelurahan,alamat_kecamatan,alamat_kota,provinsi,no_telp])  
                    cursor.execute('INSERT INTO PETUGAS_FASKES values(%s)',[username])
                    
                    request.session['username']=username
                    request.session['role'] = 'Petugas Faskes'
                    return redirect('user:dashboard')
                except(InternalError):
                    messages.error(request, "Username sudah terdaftar!")
                    return redirect('user:daftar_petugas_faskes')

    context = {
        "create_petugas_faskes": formRegistPetugasFaskes()
    }
    return render(request, 'daftar_petugas_faskes.html', context)

def daftar_supplier(request):
    if request.method=="POST" :
        form = formRegistSupplier(request.POST)
        if form.is_valid():
            nama_organisasi = form.cleaned_data.get('nama_organisasi')
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            alamat_kelurahan = form.cleaned_data.get('alamat_kelurahan')
            alamat_kecamatan = form.cleaned_data.get('alamat_kecamatan')
            alamat_kota = form.cleaned_data.get('alamat_kota')
            provinsi = form.cleaned_data.get('provinsi')
            no_telp = form.cleaned_data.get('no_telp')

            with connection.cursor() as cursor:
                try:
                    cursor.execute('INSERT INTO PENGGUNA values(%s,%s,%s,%s,%s,%s,%s,%s)',[username,password,nama_lengkap,alamat_kelurahan,alamat_kecamatan,alamat_kota,provinsi,no_telp])  
                    cursor.execute('INSERT INTO supplier values(%s,%s)',[username,nama_organisasi])

                    request.session['username']=username
                    request.session['role'] = 'Supplier'
                    return redirect('user:dashboard')
                except(InternalError):
                    messages.error(request, "Username sudah terdaftar!")
                    return redirect('user:daftar_supplier')

    context = {
        "create_supplier": formRegistSupplier()
        }
    return render(request, 'daftar_supplier.html', context)

def daftar_petugas_distribusi(request):
    if request.method=="POST" :
        form = formRegistPetugasDistribusi(request.POST)
        if form.is_valid():
            no_sim = form.cleaned_data.get('no_sim')
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            alamat_kelurahan = form.cleaned_data.get('alamat_kelurahan')
            alamat_kecamatan = form.cleaned_data.get('alamat_kecamatan')
            alamat_kota = form.cleaned_data.get('alamat_kota')
            provinsi = form.cleaned_data.get('provinsi')
            no_telp = form.cleaned_data.get('no_telp')

            with connection.cursor() as cursor:
                try:
                    cursor.execute('INSERT INTO PENGGUNA values(%s,%s,%s,%s,%s,%s,%s,%s)',[username,password,nama_lengkap,alamat_kelurahan,alamat_kecamatan,alamat_kota,provinsi,no_telp])  
                    cursor.execute('INSERT INTO PETUGAS_DISTRIBUSI values(%s,%s)',[username,no_sim])

                    request.session['username']=username
                    request.session['role'] = 'Petugas Distribusi'
                    return redirect('user:dashboard')
                except(InternalError):
                    messages.error(request, "Username sudah terdaftar!")
                    return redirect('user:daftar_petugas_distribusi')

    context = {
        "create_petugas_distribusi": formRegistPetugasDistribusi()
        }
    return render(request, 'daftar_petugas_distribusi.html', context)



