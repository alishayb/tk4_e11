from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register',views.register,name='register'),
    path('dashboard/',views.dashboard,name='dashboard'),
    path('daftarAdminSatgas/', views.daftar_admin_satgas, name='daftar_admin_satgas'),
    path('daftarPetugasFaskes/', views.daftar_petugas_faskes, name='daftar_petugas_faskes'),
    path('daftarSupplier/', views.daftar_supplier, name='daftar_supplier'),
    path('daftarPetugasDistribusi/', views.daftar_petugas_distribusi, name='daftar_petugas_distribusi')

]
