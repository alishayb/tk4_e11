from django import forms

class formLogin(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())

class formRegistRole(forms.Form):
    ROLE_1 = 'Admin Satgas'
    ROLE_2 = 'Supplier'
    ROLE_3 = 'Petugas Distribusi'
    ROLE_4 = 'Petugas Faskes'
    ROLE = (
		(ROLE_1, 'Admin Satgas'),
		(ROLE_2, 'Supplier'),
		(ROLE_3, 'Petugas Distribusi'),
        (ROLE_4, 'Petugas Faskes'),
	)
    role = forms.ChoiceField(choices=ROLE)

PROVINSI= [
    ('Aceh', 'Aceh'),
    ('Sumatra Utara', 'Sumatra Utara'),
    ('Sumatra Barat', 'Sumatra Barat'),
    ('Riau', 'Riau'),
    ('Kepulauan Riau', 'Kepulauan Riau'),
    ('Jambi', 'Jambi'),
    ('Bengkulu', 'Bengkulu'),
    ('Sumatra Selatan', 'Sumatra Selatan'),
    ('Kepulauan Bangka Belitung', 'Kepulauan Bangka Belitung'),
    ('Lampung', 'Lampung'),
    ('Banten', 'Banten'),
    ('Jawa Barat', 'Jawa Barat'),
    ('DKI Jakarta', 'DKI Jakarta'),
    ('Jawa Tengah', 'Jawa Tengah'),
    ('Daerah Istimewa Yogyakarta', 'Daerah Istimewa Yogyakarta'),
    ('Jawa Timur', 'Jawa Timur'),
    ('Bali', 'Bali'),
    ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
    ('Kalimantan Barat', 'Kalimantan Barat'),
    ('Kalimantan Selatan', 'Kalimantan Selatan'),
    ('Kalimantan Tengah', 'Kalimantan Tengah'),
    ('Kalimantan Timur', 'Kalimantan Timur'),
    ('Kalimantan Utara', 'Kalimantan Utara'),
    ('Gorontalo', 'Gorontalo'),
    ('Sulawesi Barat', 'Sulawesi Barat'),
    ('Sulawesi Selatan', 'Sulawesi Selatan'),
    ('Sulawesi Tengah', 'Sulawesi Tengah'),
    ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
    ('Sulawesi Utara', 'Sulawesi Utara'),
    ('Maluku', 'Maluku'),
    ('Maluku Utara', 'Maluku Utara'),
    ('Papua Barat', 'Papua Barat'),
    ('Papua', 'Papua')
    ]


class formRegistAdminSatgas(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    nama_lengkap = forms.CharField(required=True)
    alamat_kelurahan = forms.CharField()
    alamat_kecamatan = forms.CharField()
    alamat_kota = forms.CharField()
    provinsi = forms.ChoiceField(choices = PROVINSI)
    no_telp = forms.IntegerField()


class formRegistSupplier(forms.Form):
    nama_organisasi = forms.CharField(required=True)
    username = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    nama_lengkap = forms.CharField(required=True)
    alamat_kelurahan = forms.CharField()
    alamat_kecamatan = forms.CharField()
    alamat_kota = forms.CharField()
    provinsi = forms.ChoiceField(choices = PROVINSI)
    no_telp = forms.IntegerField()

class formRegistPetugasDistribusi(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    nama_lengkap = forms.CharField(required=True)
    alamat_kelurahan = forms.CharField()
    alamat_kecamatan = forms.CharField()
    alamat_kota = forms.CharField()
    provinsi = forms.ChoiceField(choices = PROVINSI)
    no_telp = forms.IntegerField()
    no_sim = forms.IntegerField()

class formRegistPetugasFaskes(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    nama_lengkap = forms.CharField(required=True)
    alamat_kelurahan = forms.CharField()
    alamat_kecamatan = forms.CharField()
    alamat_kota = forms.CharField()
    provinsi = forms.ChoiceField(choices = PROVINSI)
    no_telp = forms.IntegerField()
