from django.urls import path
from . import views

app_name = 'gilangcy'

urlpatterns = [
    path('createPesananSumberDaya/', views.createPesananSumberDaya, name='createPesananSumberDaya'),
    path('readPesananSumberDaya/',views.readPesananSumberDaya, name='readPesananSumberDaya'),
    path('deletePesananSumberDaya/<int:delete_id>/',views.deletePesananSumberDaya, name='deletePesananSumberDaya'),
    path('detailPesananSumberDaya/<int:detail_id>/',views.detailPesananSumberDaya, name='detailPesananSumberDaya'),
    path('riwayatStatusPesananSumberDaya/<int:pesanan_id>/',views.riwayatStatusPesananSumberDaya, name='riwayatStatusPesananSumberDaya'),
    path('updatePesananSumberDaya/<int:pesanan_id>/',views.updatePesananSumberDaya, name='updatePesananSumberDaya'),
    path('createRiwayatStatusPesanan/<int:pesanan_id>/<str:kode>',views.createRiwayatStatusPesanan, name='createRiwayatStatusPesanan'),
    path('readStokFaskes/',views.readStokFaskes, name='readStokFaskes'),
    path('createStokFaskes/',views.createStokFaskes, name='createStokFaskes'),
    path('deleteStokFaskes/<str:kode_faskes>/<str:item>',views.deleteStokFaskes, name='deleteStokFaskes'),
    path('updateStokFaskes/<str:kode_faskes>/<str:item>',views.updateStokFaskes, name='updateStokFaskes')
]
