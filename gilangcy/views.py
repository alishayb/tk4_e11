from django.shortcuts import redirect, render
from django.db import connection
from django.http import request
from django.contrib import messages

# Create your views here.
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def createPesananSumberDaya(request):
    if(request.session['role']!='Admin Satgas'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        if request.method == "POST":
            button = request.POST.get('button')
            new_no_transaksi = request.POST.get('new_no_transaksi')[3:]
            username_supplier = request.POST.get('data_supplier')
            admin_satgas = request.POST.get('admin_satgas')

            if(button == 'simpan'):
                
                insert_into_pesanan_sumber_daya = 'INSERT INTO PESANAN_SUMBER_DAYA VALUES(%s,%s,NULL)'
                tuple_query = [new_no_transaksi,admin_satgas]
                cursor.execute(insert_into_pesanan_sumber_daya,tuple_query)  

                count_total_harga ='SELECT SUM(harga_kumulatif) FROM DAFTAR_ITEM WHERE no_transaksi_sumber_daya = %s'
                tuple_query = [new_no_transaksi]
                cursor.execute(count_total_harga,tuple_query)
                total_harga = cursor.fetchall()[0][0]

                update_total_harga ='UPDATE PESANAN_SUMBER_DAYA SET total_harga = %s WHERE nomor_pesanan = %s'
                tuple_query = [total_harga,new_no_transaksi]
                cursor.execute(update_total_harga,tuple_query)

                return redirect('gilangcy:readPesananSumberDaya')

            elif(button == 'tambah'):

                select_list_item = 'SELECT kode FROM ITEM_SUMBER_DAYA where username_supplier=%s'
                tuple_query = [username_supplier] 
                cursor.execute(select_list_item,tuple_query)  
                list_item = cursor.fetchall()

                list_item_clean =[]
                for data in list_item:
                    list_item_clean.append(data[0])

                step = request.POST.get('step')

                if(step == "1"): 

                    context = {'supplier':username_supplier, 'new_no_transaksi':new_no_transaksi, 'list_item_supplier':list_item_clean,'step':2, 'admin':admin_satgas}

                elif (step == "2") :
                   
                    jumlah_item = request.POST.get('jumlah_item')
                    item = request.POST.get('data_item')

                    select_nomor_item = 'SELECT count(*) FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s;'
                    tuple_query = [new_no_transaksi]
                    cursor.execute(select_nomor_item,tuple_query)
                    nomor_item = cursor.fetchall()[0][0]

                    try :
                        insert_into_daftar_item = 'INSERT INTO DAFTAR_ITEM VALUES (%s,%s,%s,%s,NULL,NULL)'
                        tuple_query = [new_no_transaksi,nomor_item,jumlah_item,item]
                        cursor.execute(insert_into_daftar_item,tuple_query)

                    except :
                        pass

                    select_daftar_item_dipesan = 'SELECT d.no_urut,i.nama,i.harga_satuan,d.jumlah_item FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s ORDER BY(d.no_urut)'
                    tuple_query = [new_no_transaksi]
                    cursor.execute(select_daftar_item_dipesan,tuple_query)
                    daftar_item_dipesan = cursor.fetchall()

                    context = {'supplier':username_supplier, 'new_no_transaksi':new_no_transaksi, 'list_item_supplier':list_item_clean,'step':2, 'daftar_item_dipesan':daftar_item_dipesan, 'admin':admin_satgas}

            else :

                select_list_item = 'SELECT kode FROM ITEM_SUMBER_DAYA where username_supplier=%s'
                tuple_query = [username_supplier] 
                cursor.execute(select_list_item,tuple_query)  
                list_item = cursor.fetchall()

                list_item_clean =[]
                for data in list_item:
                    list_item_clean.append(data[0])

                jumlah_item = request.POST.get('jumlah_item')
                item = request.POST.get('data_item')

                delete_item = 'DELETE FROM DAFTAR_ITEM WHERE no_transaksi_sumber_daya=%s AND no_urut=%s;'
                tuple_query = [new_no_transaksi,button]
                cursor.execute(delete_item,tuple_query)

                
                select_daftar_item_dipesan = 'SELECT d.no_urut,i.nama,i.harga_satuan,d.jumlah_item FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s ORDER BY(d.no_urut)'
                tuple_query = [new_no_transaksi]
                cursor.execute(select_daftar_item_dipesan,tuple_query)
                daftar_item_dipesan = cursor.fetchall()
                
                context = {'supplier':username_supplier, 'new_no_transaksi':new_no_transaksi, 'list_item_supplier':list_item_clean,'step':2, 'daftar_item_dipesan':daftar_item_dipesan, 'admin':admin_satgas}
   
        else :
            admin_satgas = request.session['username']
            cursor.execute('DELETE FROM TRANSAKSI_SUMBER_DAYA WHERE total_berat IS NULL AND total_item IS NULL') 
            step = 1
            cursor.execute('INSERT INTO TRANSAKSI_SUMBER_DAYA(tanggal,total_berat,total_item) VALUES ((SELECT NOW()),NULL,NULL)')  
            fetch = "SELECT nomor FROM TRANSAKSI_SUMBER_DAYA WHERE total_berat IS NULL AND total_item IS NULL"
            cursor.execute(fetch)
            new_no_transaksi = cursor.fetchall()
            new_no_transaksi = new_no_transaksi[0][0]
        
            fetch = "SELECT DISTINCT s.username FROM ITEM_SUMBER_DAYA I, SUPPLIER S WHERE s.username=i.username_supplier ORDER BY s.username ASC"
            cursor.execute(fetch)
            list_data = cursor.fetchall()
            data_supplier =[]
            for data in list_data:
                data_supplier.append(data[0])
            row = dictfetchall(cursor)
            context = {'data_supplier':data_supplier, 'new_no_transaksi':new_no_transaksi, 'step':1, 'admin':admin_satgas}
        return render(request,'createPesananSumberDaya.html',context)

def updatePesananSumberDaya(request,pesanan_id):
    if(request.session['role']!='Admin Satgas'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        if request.method == "POST" :
            button = request.POST.get('button')
            no_transaksi = pesanan_id
            username_supplier = request.POST.get('data_supplier')
            admin_satgas = request.POST.get('admin_satgas')

            if(button == 'simpan'):
                
                delete_pesanan_sumber_daya = "DELETE FROM PESANAN_SUMBER_DAYA WHERE nomor_pesanan =%s "
                tuple_query = [pesanan_id]
                cursor.execute(delete_pesanan_sumber_daya, tuple_query)

                insert_into_pesanan_sumber_daya = 'INSERT INTO PESANAN_SUMBER_DAYA VALUES(%s,%s,NULL)'
                tuple_query = [no_transaksi,admin_satgas]
                cursor.execute(insert_into_pesanan_sumber_daya,tuple_query)  

                count_total_harga ='SELECT SUM(harga_kumulatif) FROM DAFTAR_ITEM WHERE no_transaksi_sumber_daya = %s'
                tuple_query = [no_transaksi]
                cursor.execute(count_total_harga,tuple_query)
                total_harga = cursor.fetchall()[0][0]

                update_total_harga ='UPDATE PESANAN_SUMBER_DAYA SET total_harga = %s WHERE nomor_pesanan = %s'
                tuple_query = [total_harga,no_transaksi]
                cursor.execute(update_total_harga,tuple_query)

                return redirect('gilangcy:readPesananSumberDaya')

            elif(button == 'tambah'):

                select_list_item = 'SELECT kode FROM ITEM_SUMBER_DAYA where username_supplier=%s'
                tuple_query = [username_supplier] 
                cursor.execute(select_list_item,tuple_query)  
                list_item = cursor.fetchall()

                list_item_clean =[]
                for data in list_item:
                    list_item_clean.append(data[0])
                   
                jumlah_item = request.POST.get('jumlah_item')
                item = request.POST.get('data_item')

                select_nomor_item = 'SELECT count(*) FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s;'
                tuple_query = [no_transaksi]
                cursor.execute(select_nomor_item,tuple_query)
                nomor_item = cursor.fetchall()[0][0]

                try :
                    insert_into_daftar_item = 'INSERT INTO DAFTAR_ITEM VALUES (%s,%s,%s,%s,NULL,NULL)'
                    tuple_query = [no_transaksi,nomor_item,jumlah_item,item]
                    cursor.execute(insert_into_daftar_item,tuple_query)
                except :
                    pass

                select_daftar_item_dipesan = 'SELECT d.no_urut,i.nama,i.harga_satuan,d.jumlah_item FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s ORDER BY(d.no_urut)'
                tuple_query = [no_transaksi]
                cursor.execute(select_daftar_item_dipesan,tuple_query)
                daftar_item_dipesan = cursor.fetchall()

                context = {'supplier':username_supplier, 'no_transaksi':no_transaksi, 'list_item_supplier':list_item_clean, 'daftar_item_dipesan':daftar_item_dipesan, 'admin':admin_satgas}

            else :

                select_list_item = 'SELECT kode FROM ITEM_SUMBER_DAYA where username_supplier=%s'
                tuple_query = [username_supplier] 
                cursor.execute(select_list_item,tuple_query)  
                list_item = cursor.fetchall()

                list_item_clean =[]
                for data in list_item:
                    list_item_clean.append(data[0])

                jumlah_item = request.POST.get('jumlah_item')
                item = request.POST.get('data_item')

                delete_item = 'DELETE FROM DAFTAR_ITEM WHERE no_transaksi_sumber_daya=%s AND no_urut=%s;'
                tuple_query = [no_transaksi,button]
                cursor.execute(delete_item,tuple_query)

                
                select_daftar_item_dipesan = 'SELECT d.no_urut,i.nama,i.harga_satuan,d.jumlah_item FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s ORDER BY(d.no_urut)'
                tuple_query = [no_transaksi]
                cursor.execute(select_daftar_item_dipesan,tuple_query)
                daftar_item_dipesan = cursor.fetchall()
                
                context = {'supplier':username_supplier, 'no_transaksi':no_transaksi, 'list_item_supplier':list_item_clean, 'daftar_item_dipesan':daftar_item_dipesan, 'admin':admin_satgas}
   

        else :
            tuple_query = [pesanan_id]

            select_admin = 'SELECT username_admin_satgas FROM PESANAN_SUMBER_DAYA WHERE nomor_pesanan = %s;'
            cursor.execute(select_admin,tuple_query)
            admin_satgas = cursor.fetchall()[0][0]

            select_supplier = 'SELECT DISTINCT i.username_supplier FROM TRANSAKSI_SUMBER_DAYA tsd, DAFTAR_ITEM d, ITEM_SUMBER_DAYA i WHERE tsd.nomor = d.no_transaksi_sumber_daya AND d.kode_item_sumber_daya = i.kode AND tsd.nomor = %s ;'
            cursor.execute(select_supplier,tuple_query)
            username_supplier =  cursor.fetchall()[0][0]

            select_daftar_item_dipesan = 'SELECT d.no_urut,i.nama,i.harga_satuan,d.jumlah_item FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s ORDER BY(d.no_urut)'
            cursor.execute(select_daftar_item_dipesan,tuple_query)
            daftar_item_dipesan = cursor.fetchall()

            select_list_item = 'SELECT kode FROM ITEM_SUMBER_DAYA where username_supplier = %s;'
            tuple_query = [username_supplier] 
            cursor.execute(select_list_item,tuple_query)  
            list_item = cursor.fetchall()

            list_item_clean =[]
            for data in list_item:
                list_item_clean.append(data[0])

            context ={'supplier' : username_supplier ,'no_transaksi':pesanan_id,'admin':admin_satgas, 'daftar_item_dipesan':daftar_item_dipesan, 'list_item_supplier':list_item_clean}
            



    return render(request,'updatePesananSumberDaya.html',context)

def readPesananSumberDaya(request):
    if(request.session['role']=='Petugas Faskes' or request.session['role']=='Petugas Distribusi'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        role = request.session['role']
        if(role == 'Petugas Distribusi' and role == 'Petugas Faskes'):
            return redirect('user:dashboard')
        username_pengguna  = request.session['username']
        if(role == 'Admin Satgas'):
            cursor.execute('SELECT tsd.nomor, psd.username_admin_satgas, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rw.kode_status_pesanan FROM TRANSAKSI_SUMBER_DAYA TSD, PESANAN_SUMBER_DAYA PSD, RIWAYAT_STATUS_PESANAN RW WHERE tsd.nomor = psd.nomor_pesanan AND tsd.nomor = rw.no_pesanan AND rw.kode_status_pesanan IN (SELECT rww.kode_status_pesanan FROM RIWAYAT_STATUS_PESANAN rww, STATUS_PESANAN sp WHERE rww.kode_status_pesanan = sp.kode AND rww.no_pesanan = tsd.nomor ORDER BY sp.step DESC LIMIT 1) AND psd.username_admin_satgas = %s ORDER BY tsd.nomor',[username_pengguna])
        elif(role == 'Supplier'):
            cursor.execute('SELECT tsd.nomor, psd.username_admin_satgas, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rw.kode_status_pesanan FROM TRANSAKSI_SUMBER_DAYA TSD, PESANAN_SUMBER_DAYA PSD, RIWAYAT_STATUS_PESANAN RW WHERE tsd.nomor = psd.nomor_pesanan AND tsd.nomor = rw.no_pesanan AND rw.kode_status_pesanan IN (SELECT rww.kode_status_pesanan FROM RIWAYAT_STATUS_PESANAN rww, STATUS_PESANAN sp WHERE rww.kode_status_pesanan = sp.kode AND rww.no_pesanan = tsd.nomor ORDER BY sp.step DESC LIMIT 1);')
        
        list_pesanan_sumber_daya = cursor.fetchall()
        context ={'daftar':list_pesanan_sumber_daya, 'role':role}
        row = dictfetchall(cursor)
        return render(request,'readPesananSumberDaya.html',context)

def deletePesananSumberDaya(request, delete_id):
    if(request.session['role']!='Admin Satgas'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM TRANSAKSI_SUMBER_DAYA WHERE nomor=%s',[delete_id])
        return redirect('gilangcy:readPesananSumberDaya')


def detailPesananSumberDaya(request,detail_id):
    if(request.session['role']=='Petugas Faskes' or request.session['role']=='Petugas Distribusi'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        cursor.execute('SELECT tsd.nomor, psd.username_admin_satgas, tsd.tanggal, psd.total_harga FROM TRANSAKSI_SUMBER_DAYA TSD, PESANAN_SUMBER_DAYA PSD, RIWAYAT_STATUS_PESANAN RW WHERE tsd.nomor = psd.nomor_pesanan AND tsd.nomor = rw.no_pesanan AND tsd.nomor = %s',[detail_id])
        pesanan = cursor.fetchall()[0]
        cursor.execute('SELECT i.username_supplier,d.no_urut,i.nama, i.harga_satuan,d.jumlah_item FROM daftar_item d, item_sumber_daya i WHERE d.kode_item_sumber_daya=i.kode AND d.no_transaksi_sumber_daya =%s ORDER BY d.no_urut;',[detail_id])
        daftar_item = cursor.fetchall()
        context = {'pesanan':pesanan,'daftar_item':daftar_item}
        return render(request,'detailPesananSumberDaya.html',context)

def riwayatStatusPesananSumberDaya(request,pesanan_id):
    if(request.session['role']=='Petugas Faskes' or request.session['role']=='Petugas Distribusi'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        select_list_riwayat = 'SELECT rw.kode_status_pesanan, st.nama, rw.username_supplier, rw.tanggal FROM RIWAYAT_STATUS_PESANAN rw , STATUS_PESANAN st WHERE rw.kode_status_pesanan = st.kode AND rw.no_pesanan = %s'
        tuple_query = [pesanan_id]
        cursor.execute(select_list_riwayat,tuple_query)
        list_riwayat = cursor.fetchall()
        context = {'pesanan_id':pesanan_id,'list_riwayat':list_riwayat}

        return render(request,'readRiwayatStatusPesanan.html',context)

def createRiwayatStatusPesanan(request,pesanan_id,kode):
    if(request.session['role']!='Supplier'):
        return redirect('user:dashboard')

    username_supplier =request.session['username']
    with connection.cursor() as cursor:
        create_riwayat_status_pesanan = 'INSERT INTO riwayat_status_pesanan VALUES(%s,%s,%s,(SELECT NOW()))'
        tuple_query = [kode,pesanan_id,username_supplier]
        cursor.execute(create_riwayat_status_pesanan,tuple_query)
    return redirect('gilangcy:readPesananSumberDaya')
         
def readStokFaskes(request):
    role = request.session['role']
    if(role=='Supplier' or role=='Petugas Distribusi'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor :

        username_pengguna = request.session['username']
        tuple_query = [username_pengguna]

        if(role == 'Admin Satgas'):
            select_list_stok_faskes = 'SELECT SF.kode_faskes, TF.nama_tipe, ISD.nama, SF.jumlah FROM STOK_FASKES SF, ITEM_SUMBER_DAYA ISD, FASKES F, TIPE_FASKES TF WHERE SF.kode_faskes = F.kode_faskes_nasional AND SF.kode_item_sumber_daya = ISD.kode AND TF.kode = F.kode_tipe_faskes'
            cursor.execute(select_list_stok_faskes)
        elif(role == 'Petugas Faskes'):
            select_list_stok_faskes = 'SELECT SF.kode_faskes, TF.nama_tipe, ISD.nama, SF.jumlah FROM STOK_FASKES SF, ITEM_SUMBER_DAYA ISD, FASKES F, TIPE_FASKES TF WHERE SF.kode_faskes = F.kode_faskes_nasional AND SF.kode_item_sumber_daya = ISD.kode AND TF.kode = F.kode_tipe_faskes AND F.username_petugas = %s'
            cursor.execute(select_list_stok_faskes,tuple_query)
        
        list_stok_faskes = cursor.fetchall()
        context ={'list_stok_faskes':list_stok_faskes, 'role':role}
        
        return render(request,'readStokFaskes.html',context)

def createStokFaskes(request):
    if(request.session['role']!='Admin Satgas'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor :
        if request.method == 'POST':
            step = request.POST.get('step')
            kode_faskes = request.POST.get('kode_faskes')
            if step == '1' :
                select_faskes = 'SELECT TF.nama_tipe FROM FASKES F, TIPE_FASKES TF WHERE F.kode_tipe_faskes = TF.kode AND F.kode_faskes_nasional = %s'
                tuple_query = [kode_faskes]
                cursor.execute(select_faskes,tuple_query)
                faskes = cursor.fetchall()[0][0]

                select_list_item = 'SELECT kode FROM ITEM_SUMBER_DAYA;'
                cursor.execute(select_list_item)
                list_item = cursor.fetchall()
               
                context = {'faskes':faskes, 'list_item':list_item, 'kode_faskes':kode_faskes,'step':2}
                
            elif step == '2' : 
                faskes = request.POST.get('faskes')
                kode_item = request.POST.get('list_item')

                select_item = 'SELECT nama FROM ITEM_SUMBER_DAYA WHERE kode = %s;'
                tuple_query = [kode_item]
                cursor.execute(select_item,tuple_query)
                item = cursor.fetchall()[0][0]
                
                context ={'faskes':faskes, 'kode_faskes':kode_faskes,'kode_item':kode_item,'item':item, 'step':3}
            else :
                kode_item = request.POST.get('kode_item')
                jumlah = request.POST.get('jumlah_item')

                insert_stok_faskes = 'INSERT INTO STOK_FASKES VALUES(%s,%s,%s)'
                tuple_query = [kode_faskes,kode_item,jumlah]
                cursor.execute(insert_stok_faskes,tuple_query)
                
                return redirect('gilangcy:readStokFaskes')
                

        else:
            select_list_faskes = 'SELECT kode_faskes_nasional FROM FASKES;'
            cursor.execute(select_list_faskes)  
            list_faskes = cursor.fetchall()
            context= {'list_faskes':list_faskes,'step':1}

    return render(request,'createStokFaskes.html',context)

def deleteStokFaskes(request, kode_faskes,item):
    if(request.session['role']!='Admin Satgas'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        select_kode_item = 'SELECT kode FROM ITEM_SUMBER_DAYA WHERE nama =%s'
        tuple_query = [item]
        cursor.execute(select_kode_item,tuple_query)
        kode_item_sumber_daya = cursor.fetchall()[0][0]
        
        delete_stok_faskes = 'DELETE FROM STOK_FASKES WHERE kode_faskes=%s AND kode_item_sumber_daya=%s'
        tuple_query = [kode_faskes,kode_item_sumber_daya]
        cursor.execute(delete_stok_faskes,tuple_query)
        return redirect('gilangcy:readStokFaskes')
    

def updateStokFaskes(request, kode_faskes,item):
    if(request.session['role']!='Admin Satgas'):
        return redirect('user:dashboard')
    with connection.cursor() as cursor:
        if request.method == 'POST':
            return redirect('gilangcy:readStokFaskes')
        else :
            
            select_kode_item = 'SELECT kode FROM ITEM_SUMBER_DAYA WHERE nama =%s'
            tuple_query = [item]
            cursor.execute(select_kode_item,tuple_query)
            kode_item_sumber_daya = cursor.fetchall()[0][0]

            select_faskes = 'SELECT TF.nama_tipe FROM FASKES F, TIPE_FASKES TF WHERE F.kode_tipe_faskes = TF.kode AND F.kode_faskes_nasional = %s'
            tuple_query = [kode_faskes]
            cursor.execute(select_faskes,tuple_query)
            faskes = cursor.fetchall()[0][0]

            select_jumlah_item = "SELECT jumlah FROM STOK_FASKES WHERE kode_faskes=%s AND kode_item_sumber_daya=%s"
            tuple_query = [kode_faskes,kode_item_sumber_daya]
            cursor.execute(select_jumlah_item,tuple_query)
            jumlah_item =cursor.fetchall()[0][0]

            context={'faskes':faskes, 'kode_faskes':kode_faskes,'kode_item':kode_item_sumber_daya,'item':item,'jumlah':jumlah_item}
    return render(request,'updateStokFaskes.html',context)